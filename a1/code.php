<?php

class Person {
	public function __construct($first_name, $middle_name, $last_name){
		$this->first_name = $first_name;
		$this->middle_name = $middle_name;
		$this->last_name = $last_name;
	}

	public function print_name(){
		return "Your full name is $this->first_name $this->middle_name $this->last_name";
	}
}

class Developer extends Person {
	public function print_name(){
		return "Your name is $this->first_name $this->middle_name $this->last_name and you are a developer";
	}
}

class Engineer extends Person {
	public function print_name(){
		return "You are an engineer named $this->first_name $this->middle_name $this->last_name";
	}
}

$person = new Person("Senku", null, "Ishigami");
$developer = new Developer("John", "Finch", "Smith");
$engineer = new Engineer("Harold", "Myers", "Reese");