<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S03 - Activity</title>
</head>
<body>
	<h1>Person</h1>
	<?php echo $person->print_name() . "."; ?>

	<h1>Developer</h1>
	<?php echo $developer->print_name() . "."; ?>

	<h1>Engineer</h1>
	<?php echo $engineer->print_name() . "."; ?>
</body>
</html>