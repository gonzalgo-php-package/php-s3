<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S03</title>
</head>
<body>
	<h1>Objects from Variables</h1>
	<pre><?php print_r($building_obj); ?></pre>

	<h1>Objects from Classes</h1>
	<pre><?php print_r($building); ?></pre>
	<pre><?php print_r($building2); ?></pre>
	<pre><?php print_r($building->print_name()); ?></pre>
	<pre><?= $building2->print_name(); ?></pre>

	<h1>Inheritance</h1>
	<pre><?php print_r($condominium); ?></pre>

	<h1>Polymorphism</h1>
	<pre><?= $condominium->print_name(); ?></pre>
</body>
</html>