<?php

//[SECTION] Objects as varibles
$building_obj = (object) [
	"name" => "Caswynn Building",
	"floors" => 8,
	"address" => (object)[
		"barangay" => "Sacred Heart",
		"city" => "Quezon City",
		"country" => "Philippines"
	]
];

//[SECTION] Objects from Classes

class Building {
	public $name;
	public $floors;
	public $address;

	//constructor
	//A constructor is used during the creation of an object to provide the initial values of each property

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//methods
	public function print_name(){
		return "The name of the building is $this->name";
	}
}

class Condominium extends Building {
	//$name. $floors, and $address are inherited from the Building class to this class
	//It means that condominiums also have a name, floors, and address property, as well as the print_name() method

	public function print_name(){
		return "The name of the condominiun is $this->name";
	}
}

$building = new Building("Cawynn Building", 8, "Timog Avenue, Quezon City, Philippines");

$building2 = new Building("Manulife Building", 51, "Commonwealth Avenue, Quezon City, Philippines");

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");